﻿namespace Autovermietung
{
    public class Bus : Vehicle
    {
        public int Seats { get; set; }
        public bool HasTwoFloors { get; }
        public int Axis { get; }

        public Bus (Manufacturer manufacturer, string model, double pricePerDay, int doors, int xWheelDrive, 
            int seats, int axis = 2, bool isSelfDriving = false, bool hasTrailerHitch = false, 
            Plate plate = null, bool hasTwoFloors = false, Employee mechanic = null) : 
            base(manufacturer, model, pricePerDay, doors, xWheelDrive, isSelfDriving, hasTrailerHitch, plate, mechanic)
        {
            Seats = seats;
            HasTwoFloors = hasTwoFloors;
            Axis = axis;
        }
    }
}