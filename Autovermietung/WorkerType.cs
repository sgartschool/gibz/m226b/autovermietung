using System;

namespace Autovermietung
{
    [Serializable]
    public enum WorkerType
    {
        Cleaning, Management, Marketing, Mechanic, Reception, Sales, Service, Other
    }
}