using System;

namespace Autovermietung
{
    [Serializable]
    public class Employee : Human
    {
        public WorkerType Department { get; set; }

        public Employee(string name, Language nativeLanguage, WorkerType department = WorkerType.Other) : base(name,
            nativeLanguage)
        {
            Department = department;
        }
        public override void PrintInfo()
        {
            Console.Write($"Employee:\t{Name}\n");
            base.PrintInfo();
            Console.Write($"\t\t Department: {Department}\n");
        }
    }
}