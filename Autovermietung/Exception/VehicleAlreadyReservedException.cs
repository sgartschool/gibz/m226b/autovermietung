﻿namespace Autovermietung.Exception
{
    public class VehicleAlreadyReservedException : System.Exception
    {
        public VehicleAlreadyReservedException()
        :base($"At least one vehicle is already used by another customer at least one day during the timespan.")
        {
            
        }
        
        public VehicleAlreadyReservedException(Vehicle vehicle)
        :base($"Vehicle {vehicle.Manufacturer} {vehicle.Model} ({vehicle.Id}) " +
              $"is already used by another customer at least one day during the timespan.")
        {
            
        }
    }
}