﻿using System;

namespace Autovermietung.Exception
{
    public class InvalidLicenseException : System.Exception
    {
        public InvalidLicenseException()
        {
            
        }

        public InvalidLicenseException(Customer customer) 
            : base (String.Format($"Customer '{customer.Name}' with ID " +
                                  $"'{customer.Id}' does not have a valid License!"))
        {
            
        }
    }
}