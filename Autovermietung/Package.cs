﻿using System.Collections.Generic;

namespace Autovermietung
{
    public class Package<T>
    {
        public List<T> Objects = new List<T>();

        public Package () { }

        public void AddObject(T obj)
        {
            this.Objects.Add(obj);
        }
    }
}