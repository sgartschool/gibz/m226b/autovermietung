﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace Autovermietung
{
    // TODO: Add motorbike    
    
    internal partial class Program
    {
        static readonly string filepath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Desktop\windoof";
        const bool enableUI = true;
        
        // List of all types
        List<Car> cars;
        List<Van> vans;
        List<Bus> buses;
        List<Truck> trucks;
        List<Manufacturer> manufacturers;
        List<Contract> contracts;
        List<Customer> customers;
        List<Employee> employees;
        // List<Equipment> equipment;
        List<License> licenses;
        List<Plate> plates;

        public static void Main(string[] args)
        {
            // Launch Main application
            new Program().Main();
        }
        
        public void Main()
        {
            Console.WriteLine("Welcome");

            if (System.IO.File.Exists(filepath + "cars.json")) // TODO: Check all files
            {
                #region Import Data from files

                cars = JsonConvert.DeserializeObject<List<Car>>(File.ReadAllText(filepath + "cars.json"));
                vans = JsonConvert.DeserializeObject<List<Van>>(File.ReadAllText(filepath + "vans.json"));
                buses = JsonConvert.DeserializeObject<List<Bus>>(File.ReadAllText(filepath + "buses.json"));
                trucks = JsonConvert.DeserializeObject<List<Truck>>(File.ReadAllText(filepath + "trucks.json"));
                manufacturers = JsonConvert.DeserializeObject<List<Manufacturer>>(File.ReadAllText(filepath + "manufacturers.json"));
                contracts = JsonConvert.DeserializeObject<List<Contract>>(File.ReadAllText(filepath + "contracts.json"));
                customers = JsonConvert.DeserializeObject<List<Customer>>(File.ReadAllText(filepath + "customers.json"));
                employees = JsonConvert.DeserializeObject<List<Employee>>(File.ReadAllText(filepath + "employees.json"));
                // equipment = JsonConvert.DeserializeObject<List<Equipment>>(File.ReadAllText(filepath + "equipment.json"));
                licenses = JsonConvert.DeserializeObject<List<License>>(File.ReadAllText(filepath + "licenses.json"));
                plates = JsonConvert.DeserializeObject<List<Plate>>(File.ReadAllText(filepath + "plates.json"));

                #endregion
            }
            else
            {
                Console.Write("No existing data has been found. This application will now generate new data!\n");

                #region Sample Data

                cars = new List<Car>();
                vans = new List<Van>();
                buses = new List<Bus>();
                trucks = new List<Truck>();
                manufacturers = new List<Manufacturer>();
                contracts = new List<Contract>();
                customers = new List<Customer>();
                employees = new List<Employee>();
                // equipment = new List<Equipment>();
                licenses = new List<License>();
                plates = new List<Plate>();
                
                Manufacturer volkswagen = new Manufacturer("Volkswagen");
                Manufacturer subaru = new Manufacturer("Subaru");
                Manufacturer tesla = new Manufacturer("Tesla");
                Manufacturer volvo = new Manufacturer("Volvo");
                Manufacturer mercedes = new Manufacturer("Mercedes Benz");
                Manufacturer man = new Manufacturer("Man");

                Equipment childSeat = new Equipment("Child seat", "A seat extension to increase the height of the seat.");
                Equipment dashCam = new Equipment("Dash cam", "A camera attached to the windshield to film potential frauds or accidents.");
                Equipment phoneHolder = new Equipment("Phone holder", "Plug this gadget into your ac output and attach your phone");

                Customer customer0 = new Customer("Samuel Gartmann", Language.De);
                Customer customer1 = new Customer("Yanik Ammann", Language.En);

                License license0 = new License(customer0);
                License license1 = new License(customer1);

                Employee employee0 = new Employee("Max Mustermann", Language.De, WorkerType.Management);

                Plate plate0 = new Plate("ZG 123456");
                Plate plate1 = new Plate("ZH 098765");
                Plate plate2 = new Plate("LI 456123");
                Plate plate3 = new Plate("ZH 472567");
                Plate plate4 = new Plate("AG 246735");

                Car car0 = new Car(volkswagen, "3000 X", 500,4, 4, 5, false,
                    true, plate0);
                Car car1 = new Car(subaru, "Legacy", 300,4, 2, 5, false,
                    false, plate1);
                Car car2 = new Car(tesla, "Model S", 120,4, 4, 5, true);
                Car car3 = new Car(tesla, "Model X", 3005,4, 4, 5, true);
                Car car4 = new Car(subaru, "Outback", 121,4, 4, 5, true,
                    true, plate2);
                Truck truck0 = new Truck(volvo, "Super XP", 420, 2, 6,
                    8000, 8, false, true, plate3);
                Van van0 = new Van(mercedes, "Sprinter X5", 1150, 2, 2, 1000,
                    false, true, plate4);
                Bus bus0 = new Bus(man, "Eurocar V8", 1255, 2, 4, 50);

                List<Vehicle> vehicles0 = new List<Vehicle>
                {
                    car0,
                    car1,
                    car2,
                    car3,
                    car4,
                    truck0,
                    van0,
                    bus0
                };
                //
                // Contract contract0 = new Contract(customer0, employee0, vehicles0,
                //     new DateTime(2020, 10, 19), new DateTime(2020, 12, 20));
                //
                // contract0.AddEquipment(childSeat);
                // contract0.AddEquipment(dashCam);
                // contract0.AddEquipment(phoneHolder);

                customer0.License = license0;
                customer0.EmailAddress = "admin@1samuel3.com";
                // customer0.AddContract(contract0);

                customer1.License = license1;
                
                employee0.EmailAddress = "max.mustermann@autvermietung.com";

                // Add data
                manufacturers.Add(volkswagen);
                manufacturers.Add(subaru);
                manufacturers.Add(tesla);
                manufacturers.Add(volvo);
                manufacturers.Add(mercedes);
                manufacturers.Add(man);

                // equipment.Add(childSeat);
                // equipment.Add(dashCam);
                // equipment.Add(phoneHolder);

                customers.Add(customer0);
                customers.Add(customer1);

                licenses.Add(license0);
                
                employees.Add(employee0);

                plates.Add(plate0);
                plates.Add(plate1);
                plates.Add(plate2);
                plates.Add(plate3);
                plates.Add(plate4);

                cars.Add(car0);
                cars.Add(car1);
                cars.Add(car2);
                cars.Add(car3);
                cars.Add(car4);
                vans.Add(van0);
                buses.Add(bus0);
                trucks.Add(truck0);
                
                contracts.Add(new Contract(customer1, employee0, vehicles0, DateTime.Now, DateTime.Now.AddDays(3)));

                // contracts.Add(contract0);

                #endregion
            }

            while (enableUI)
            {
                // Main Menu
                Console.WriteLine("\nC Create Contract");
                Console.WriteLine("N New Object");
                Console.WriteLine("D Delete Object");
                Console.WriteLine("L List");
                Console.WriteLine("S Statistics");
                Console.WriteLine("E Save and Exit");
                Console.WriteLine("Q Quit");

                switch (Console.ReadLine().ToUpper())
                {
                    case "C":
                        CreateContract();
                        break;
                    
                    case "N":
                        CreateNew();
                        break;
                    
                    case "D":
                        DeleteObject();
                        break;

                    case "L":
                        List();
                        break;

                    case "S":
                        Statistics();
                        break;

                    case "E":
                        SaveAndExit();
                        break;

                    case "Q":
                        QuitWithoutSaving();
                        break;

                    default:
                        Console.WriteLine("Wrong selection! Please try again");
                        break;
                }
            }
        }
    }
}