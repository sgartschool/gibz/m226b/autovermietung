using System;

namespace Autovermietung
{
    [Serializable]
    public class Plate
    {
        public string Number { get; }

        public Plate(string number)
        {
            Number = number;
        }
        public void PrintInfo()
        {
            Console.Write($"Plate: {Number}\n");
        }
    }
}