﻿namespace Autovermietung
{
    public class Truck : Vehicle
    {
        public int MaxLoad { get; set; }
        public int Axis { get; }

        public Truck(Manufacturer manufacturer, string model, double pricePerDay, int doors, int xWheelDrive,
            int maxLoad, int axis, bool isSelfDriving = false, bool hasTrailerHitch = false, Plate plate = null
            , Employee mechanic = null) :
            base(manufacturer, model, pricePerDay, doors, xWheelDrive, isSelfDriving, hasTrailerHitch, plate, mechanic)
        {
            MaxLoad = maxLoad;
            Axis = axis;
        }
    }
}