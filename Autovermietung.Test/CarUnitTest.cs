﻿using System;
using System.Collections.Generic;
using Autovermietung.Exception;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Autovermietung.Test
{
    [TestClass]
    public class CarUnitTest
    {
        [TestMethod]
        public void TestEmployeeMechanic()
        {
            // arrange
            Manufacturer manufacturer = new Manufacturer("Manufacturer");
            Employee mechanic = new Employee("NotAMechanic", Language.En, WorkerType.Mechanic);
            
            // act
            Car car = new Car(manufacturer, "Speedo 2000", 4.5, 6, 8, 16, 
                true, true, null, mechanic);
            
            // assert
            Assert.AreEqual(mechanic, car.MechanicOnDamage);
        }

        [TestMethod]
        public void TestEmployeeNotMechanic()
        {
            // arrange
            Manufacturer manufacturer = new Manufacturer("Manufacturer");
            Employee mechanic = new Employee("NotAMechanic", Language.En, WorkerType.Management);
            
            // act => assert
            Assert.ThrowsException<EmployeeNotMechanicException>(() =>
                new Car(manufacturer, "Speedo 2000", 4.5, 6, 8, 16, true, 
                    true, null, mechanic));
        }
    }
}